##decaf对qemu修改代码阅读文档
#####虚拟外设线程（主线程）:
qemu 1.0之后，共有两线程，一个虚拟外设（主，处理IO），一个虚拟CPU.  
>vl.c:`main()` --> `main_loop()`  

#####虚拟cpu线程:
>vl.c:`main()` --> (`machine->init()`, machine:”pc-1.0”初始化, 对应结构在hw/pc_piix.c) hw/pc_piix.c:`pc_init_pci()` --> `pc_init1()` --> hw/pc.c:`pc_cpus_init()` --> `pc_new_cpu()` -->  (`cpu_init()`, 对应的cpu初始化，对应宏定义在target-i386/cpu.h) target-i386/helper.c:`cpu_x86_init()` --> cpus.c:`qemu_init_vcpu()` --> `qemu_tcg_init_vcpu()` --> qemu-thread-posix.c:`qemu_thread_create()` --> (新线程开始执行) cpus.c:`qemu_tcg_cpu_thread_fn()`:while(1)循环执行 --> `tcg_exec_all()` --> `tcg_cpu_exec()` --> cpu-exec.c:`cpu_exec()`:主执行循环  

#####vl.c
qemu执行入口：vl.c: `main()`.  
1.在函数`main_loop()`中，增加函数`garbage_collect_taint()`：垃圾回收.  
2.在函数`main()`中，增加switch选项：after_loadvm、load_plugin、toggle_kvm、vmi_profile（-vmi-profile）；增加函数`DECAF_init()`：decaf初始化；增加函数`do_load_plugin_internal()`：如果启动时有load_plugin选项，则载入插件；  

#####darwin-user/
删除了整个darwin-user目录，此目录是darwin操作系统的user model代码。

#####plugins/
增加了整个plugins目录，此目录是decaf的各种插件。

#####roms/
增加了整个roms目录，此目录是各种BIOS代码。

#####shared/
增加了整个shared目录，此目录是decaf主要功能的实现。

#####fpu/softfloat.h
语句“typedef int uint16;”增加条件编译。

#####hw/hw.h
增加“extern C”。

#####hw/ne2000.c
NE2000网卡模拟。  
1.在函数`ne2000_receive()`中，增加函数`DECAF_nic_receive()`：网卡收包。  
2.在函数`ne2000_ioport_write()`中，增加函数`DECAF_nic_send()`：网卡发包。  
3.在函数`ne2000_mem_writeb()`、`ne2000_mem_writew()`、`ne2000_mem_writel()`中，分别增加函数`ECAF_nic_out`复制网卡写到内存的数据。  
4.在函数`ne2000_mem_readb()`、`ne2000_mem_readw()`、`ne2000_mem_readl()`中，分别增加函数`DECAF_nic_in()`：复制网卡读内存的数据。  

#####hw/ps2.c
PS/2键盘鼠标模拟。  
1.在结构体PS2Queue中，增加taint_data[]数组，以保存按键队列的数据（shadow变量)。  
2.增加函数`ps2_queue_taint()`。  
3.增加函数`ps2_put_keycode_taint()`。  
4.在函数`ps2_put_keycode()`中，增加函数`DECAF_keystroke_place()`，假如使能按键污染，则增加运行函数`ps2_put_keycode_taint()`。  
5.在函数`ps2_read_data()`中，增加函数`DECAF_keystroke_read()`。  

#####hw/9pfs/virtio-9p-handle.c
增加一些宏定义数值。  

#####hw/ide/core.c
1.在函数`ide_data_writew()`、`ide_data_writel()`中，增加函数`taintcheck_chk_hdout()`.  
2.在函数`ide_data_readw()`、`ide_data_readl()`中，增加函数`taintcheck_chk_hdin()`.  
3.在函数`bmdma_rw_buf()`中，增加函数`taintcheck_chk_hdread()`、`taintcheck_chk_hdwrite()`.  

#####scripts/create_config
增加Source path。  

#####target-i386/
+**DECAF_target.c** 定义与i386架构相关多功能函数。  
+**DECAF_target.h** 诸多寄存器定义。  
+**log** 记录变量、函数用途。  
\#**cpu.h** 在结构体CPUX86State中，增加寄存器等变量。  
\#**helper.h** i386功能助手，增加了许多`DEF_HELPER_x()`类型的功能函数。？？？  
\#**op_helper.c** i386功能助手。  
- 函数`helper_fstenv()`中，改变了两条`stl()`的操作；  
- 增加函数`helper_DECAF_taint_patch()`，WinXP按键传播多补丁。  
- 增加函数`helper_DECAF_update_fpu()`，FPU模拟的提升的补丁。  

\#**translate.c** i386指令翻译。  
- 增加一些变量。  
- 增加函数`tcg_gen_call_cb_0()`,插入一个CallBack函数。  
- 在函数`gen_op_jmp_T0()`中，为回调`DECAF_EIP_CHECK_CB`增加函数`tcg_const_ptr()`、`tcg_gen_DECAF_checkeip()`。  
- 在函数`gen_jmp_im()`中，更新next_pc的值。  
- 在函数`gen_goto_tb()`中，回调`DECAF_INSN_END_CB`、`DECAF_OPCODE_RANGE_CB`、`BlockEndCallback`的跳转前变量赋值等处理。  
- 在函数`gen_eob()`中，回调`DECAF_INSN_END_CB`、`DECAF_OPCODE_RANGE_CB`、`BlockEndCallback`的跳转前变量赋值等处理。  
- 在函数`disas_insn()`中，诸多Decaf变量的更新；增加函数`gen_helper_DECAF_taint_cmpxchg()`，告知这是一个cmpxchg操作码；增`加gen_helper_DECAF_taint_patch()`函数；增加`gen_helper_DECAF_update_fpu()`函数；回调`DECAF_INSN_END_CB`、`DECAF_OPCODE_RANGE_CB`的跳转前变量赋值等处理。  
- 在函数`optimize_flags_init()`中，获取诸多cpu寄存器的值，并保存到Decaf的shadow变量中。  
- 增加函数`log_tcg_ir()`，处理保存TCG_IR_LOG。  
- 在函数`gen_intermediate_code_internal()`中，回调`BlockBeginCallback`、`DECAF_BLOCK_TRANS_CB`、`DECAF_INSN_BEGIN_CB`的跳转前变量赋值等处理，以及污染相关的一些操作。

#####tcg/tcg-op.h
TCG指令生成操作。  
1.增加一些tain宏定义。  
2.增加函数`tcg_gen_DECAF_checkeip(), tcg_gen_taint_qemu_ld8u(), tcg_gen_taint_qemu_ld8s(), tcg_gen_taint_qemu_ld16u(), tcg_gen_taint_qemu_ld16s(), tcg_gen_taint_qemu_ld32u(), tcg_gen_taint_qemu_ld32s(), tcg_gen_taint_qemu_ld64(), tcg_gen_taint_qemu_st8(), tcg_gen_taint_qemu_st16(), tcg_gen_taint_qemu_st32(), tcg_gen_taint_qemu_st64(), tcg_gen_DECAF_checkeip(), tcg_gen_taint_qemu_ld8u(), tcg_gen_taint_qemu_ld8s(), tcg_gen_taint_qemu_ld16u(), tcg_gen_taint_qemu_ld16s(), tcg_gen_taint_qemu_ld32u(), tcg_gen_taint_qemu_ld32s(), tcg_gen_taint_qemu_ld64(), tcg_gen_taint_qemu_st8(), tcg_gen_taint_qemu_st16(), tcg_gen_taint_qemu_st32(), tcg_gen_taint_qemu_st64()`, 这些函数是TCG指令生成操作的shadow，用于Decaf。  

#####tcg/tcg-opc.h
TCG指令的定义。增加了TCG指令的shadow定义，用于Decaf。

#####tcg/tcg.c
TCG相关的操作。修改了函数`tcg_liveness_analysis()`, 增加一个入口参数presweep，作为许多操作的开关。

#####tcg/tcg.h
增大了几个宏变量的大小。

#####tcg/i386/tcg-target.h
增加一条 条件编译。

#####tcg/i386/tcg-target.c
TCG指令向i386指令翻译的操作。  
1.增加了许多的TCG指令操作相关的函数，通过TCG调用来进行MEM相关的CallBack。  
2.通过复制`qemu_ld_helpers[]`、`qemu_st_helpers[]`数组，来建立与之对应的shadow。  
3.修改函数`tcg_out_qemu_ld()`, `tcg_out_qemu_st()`， 保存相关变量，记录下 Guest 的访存行为。  
4.增加函数`tcg_out_taint_qemu_st()`, `tcg_out_taint_qemu_ld()`，shadow以上两函数的操作。  
5.在函数`tcg_out_op()`, 增加指令翻译输出索引项。  
6.增加指令定义数组`x86_op_defs[]`的值。  

#####translate-all.c
Host指令生成。  
函数`cpu_gen_code()`、`cpu_restore_state()`中，如果污染跟踪打开，则要清空shadow变量。  

#####softmmu_template.h
软件mmu。  
在众多的`glue(glue(y, x), x)()`函数中，增加了回调`DECAF_MEM_WRITE_CB`、`DECAF_MEM_READ_CB`等内存相关的callback调用判断。

#####softmmu_header.h
头文件，增加MEM回调有关的宏定义。

#####softmmu_defs.h
软件mmu操作函数，增加对应Decaf的shadow。

#####rules.mak
编译规则相关的更改，如增加C++代码的编译。

#####qemu-options.hx
用于构建结构数据。增加了monitor的命令及其参数等。

#####osdep.h
增加inline宏定义。

#####monitor.h
mon_cmd_t结构体新建。

#####monitor.c
QEMU monitor命令操作相关。  
1.`help_cmd()`函数的修改，help命令增加显示Decaf有关的选项。  
2.为插件修改了按键发送`do_sendkey()`函数，  
3.在函数`monitor_find_command()`中，修改为，首先搜索the standard monitor cmds，然后搜索DECAF's default commands，最后搜索the plugin term cmds。  
4.在函数`monitor_find_completion()`中，增加两DECAF命令寻找和插件命令寻找。  

#####hmp.h
人工monitor界面。

#####hmp.c
增加函数`hmp_taint_debug()`, `hmp_taint()`,用于污染相关的monitor命令。

#####exec.c
虚拟页映射，翻译块 处理。  
1.增加定义部分变量。  
2.函数`code_gen_alloc()`中，增加TCG_IR_LOG即TCG中间表示(TCG指令)记录的打印，操作码BUF增大。  
3.函数`tb_alloc()`, `tb_free()`中，DECAF相关变量归零。  
4.在函数`tlb_set_page()`中, TLB回调`DECAF_TLB_EXEC_CB`的触发。  
5.函数`DECAF_physical_memory_rw()`定义。  

#####exec-all.h
qemu内部执行函数的定义。  
1.the opcode buffer即操作码BUF空间的增大。  
2.LLVM相关的宏定义和变量定义。  

#####def-helper.h
声明TCG helper功能助手函数。  
增加`DEF_HELPER_FLAGS_X`、`glue(gen_helper_, name)()`类函数的宏定义.？？？  

#####cpus.c
增加DECAF_kvm_enabled变量控制kvm的开启。  

#####cpu-exec.c
在函数`cpu_exec()`中，增加`DECAF_update_cpustate()`的调用，以更新cpustate。  

#####cpu-defs.h
CPU相关变量BUF增大。  

#####cpu-common.h
函数`cpu_physical_memory_read()`、`cpu_physical_memory_write()`， 变量buf的类型强转为(uint8_t *)。  

#####configure
编译前配置的修改，增加有C++编译选项，削减qemu支持架构的编译等。  

#####bswap.h
函数中`lduw_be_p()`、`int ldsw_be_p()`的ptr变量，类型强转成(const uint8_t *)。

#####blockdev.h
qemu Host 块设备操作函数。函数前增加extern声明。

#####block.h bitops.h bitmap.h balloon.h arch_init.h aes.h acl.h
各增加了extern "C"条件编译。

#####Makefile Makefile.objs Makefile.target
对于新增文件、C++等的编译相关的更改。

#####对于qemu 2.0的升级
- 主要头文件的归整到include.  
- 数据结构的去除或重写。  
- 增加两新的体系架构的支持。  
- 以前在一起的数据结构体或函数的定义，可能分散在不同的文件。  
- TCG:  
 - 大部分是性能的优化。  
 - 微操作的函数封装。  
 